import xml.etree.ElementTree as ET
import sys, os, subprocess

tree = ET.parse(sys.argv[1])
root = tree.getroot()
pwd = os.path.dirname(os.path.realpath(__file__))

for child in root:
    if child.tag == "project":
        try:
             path = "%s/%s" % (pwd, child.attrib['path'])
        except:
             path = "%s/%s" % (pwd, child.attrib['name'])

        p = subprocess.Popen(["git", "reset", "--hard", child.attrib['revision']], cwd=path)
        p.wait()
